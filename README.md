# Car Park

Number of Parking Space is passed as arguments to this project.

## Getting Started

The project is build using maven and Spring Framework API

By running Application.java file API services starts at localhost on port 8080

```
localhost:8080/action
```
The above link uses a POST method, recommend to use Postman App for this

1. Select POST method in the app
2. Choose Body and choose raw input
3. paste the list of actions in the input space like

```
List((PARK, 1001), (PARK, 1002), (DEPART), (DEPART), (PARK, 1003), (PARK, 1004), SMALLEST)
```

For example

![Postman Screenshot](/screenshots/postman.png "Optional title")

## Unit test

This is performed by MyTests.java