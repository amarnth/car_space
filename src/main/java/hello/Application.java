package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        if(args.length == 0)
        {
            System.out.println("Please provide number of parking space as arguments");
            System.exit(0);
        }
        SpringApplication.run(Application.class);
        // Number of Parking Space is passed as arguments to this project.
        Parking p = new Parking();
        p.setParking_space(Integer.parseInt(args[0]));
    }
}