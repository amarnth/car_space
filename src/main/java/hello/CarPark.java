package hello;

import java.util.*;

public class CarPark {

    private static Stack<Integer> stack = new Stack<>();
    private static int min;

    static String getMin() { if (!stack.isEmpty()) return Integer.toString(min); return null; }

    static void clearStack() { stack.clear(); }

    static String parkCar(String car_id) {
        String res = "Car %s Parked";
        if (Parking.getParking_space() > stack.size()) {
            Integer c_id = Integer.parseInt(car_id);
            if (stack.isEmpty()) {
                min = c_id;
                stack.push(c_id);
                return String.format(res, c_id);
            }
            if (c_id < min) {
                stack.push(2*c_id - min);
                min = c_id;
            } else {
                stack.push(c_id);
            }
            return String.format(res, c_id);
        } else {
            return null;
        }
    }

    static Stack<Integer> display(){ return stack; }

    static String departCar(){
        try {
            Integer c_id = stack.pop();
            if (c_id < min)
            {
                String S = "Car " + Integer.toString(min) + " Departed";
                min = 2*min - c_id;
                return S;
            }
            return "Car " + Integer.toString(c_id) + " Departed";
        } catch (EmptyStackException e) {
//            System.out.println("Empty Stack");
        }
        return null;
    }

}
