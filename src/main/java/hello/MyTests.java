package hello;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

public class MyTests {

    @Test
    public void testCarPark() {
        ParkingController tester = new ParkingController();
        Parking p = new Parking();
        Integer parking_space = 10;
        p.setParking_space(parking_space);

        HashMap<String, String> action_list = new HashMap<>();

        action_list.put("1003", "List((PARK, 1001), (PARK, 1002), (DEPART), (DEPART), (PARK, 1003), (PARK, 1004), SMALLEST)");
        action_list.put("1001", "List((PARK, 1011), (PARK, 1012), (DEPART), (PARK, 1001), (PARK, 1008), (DEPART), SMALLEST)");
        action_list.put("Car 1056 Departed", "List((DEPART), (PARK, 1002), (PARK, 1005), SMALLEST, (DEPART), (PARK, 1055), (PARK, 1056), (DEPART))");
        action_list.put("1002", "List((DEPART), SMALLEST)");
        action_list.put("Car Number is not 4 digits", "List((PARK, 15))");
        action_list.put("Car 1063 Parked", "List((PARK, 1057), (PARK, 1060), (PARK, 1061), (PARK, 1062), (PARK, 1063))");
        action_list.put("Parking Space Full maximum size is " + Integer.toString(parking_space), "List((PARK, 1070), (PARK, 1072))");
        action_list.put("EMPTY Parking Space", "List((DEPART), (DEPART), (DEPART), (DEPART), (DEPART), (DEPART), (DEPART), (DEPART), (DEPART), (DEPART), (DEPART))");

        // assert statements
        for (String action: action_list.keySet()){
            assertEquals(action, tester.processActionList(action_list.get(action)));
            System.out.println("PASSED");
        }
    }
}