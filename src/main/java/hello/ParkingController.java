package hello;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@RestController
public class ParkingController {

    @RequestMapping("/park") //  e.g. for individual action go to http://localhost:8080/park?id=8989
    public String parkNewCar(@RequestParam(value="id", defaultValue="") String car_id){ return CarPark.parkCar(car_id); }

    @RequestMapping("/depart") //  e.g. for individual action go to http://localhost:8080/depart
    public String departLastCar(){ return CarPark.departCar(); }

    @RequestMapping("/get") //  e.g. for individual action go to http://localhost:8080/get
    private String getMinCarNum(){
        String car_min = CarPark.getMin();
        if (car_min == null) return "Empty Parking";
        return car_min;
    }

    @RequestMapping(value = "/action", method = RequestMethod.POST)
    // Using postman post method select body and input as raw and paste the following
    // List((PARK, 1001), (PARK, 1002), (DEPART), (DEPART), (PARK, 1003), (PARK, 1004), SMALLEST)
    // Results in 1003 as the smallest
    public String processActionList(@RequestBody String actions){
        actions = actions.replace("PARK,","");
        actions = actions.replace("(","");
        actions = actions.replace(")","");
        actions = actions.replace("List","");
        actions = actions.replace(" ","");
        List<String> action_list = new ArrayList<String>(Arrays.asList(actions.split(",")));
        //
        Integer counter = 1;
        Boolean res = false;
        // respond to the list of actions only when last action is performed

        for (String action : action_list){
            if (counter == action_list.size()){
                res = true;
            }
            if (Objects.equals(action, "DEPART")){
                String dc = departLastCar();
                if(dc == null) // cannot depart the car when there is no car
                    return "EMPTY Parking Space";
                else if (res)
                    return dc;
            }
            else if(Objects.equals(action, "SMALLEST")){
                if (res) return getMinCarNum();
            }
            else {
                if(action.length() == 4){
                    String new_car = parkNewCar(action);
                    if (new_car == null)
                        return "Parking Space Full maximum size is " + CarPark.display().size();
                    else if (res)
                        return new_car;
                }else{
                    return "Car Number is not 4 digits";
                }
            }
            counter++;
        }
        return null;
    }

}
